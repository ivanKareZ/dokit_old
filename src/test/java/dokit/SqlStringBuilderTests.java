package dokit;

import static org.junit.Assert.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

import com.ivankarez.dokit.db.sql.Insert;
import com.ivankarez.dokit.db.sql.KeyValuePair;
import com.ivankarez.dokit.db.sql.Select;
import com.ivankarez.dokit.db.sql.SimpleWhereCondition;
import com.ivankarez.dokit.db.sql.SqlOperator;
import com.ivankarez.dokit.db.sql.Update;
import com.ivankarez.dokit.db.sql.WhereConditionSet;

public class SqlStringBuilderTests {
	
	@Test
	public void testSelect() {
		final Select select = new Select("table");	
		assertEquals(select.build(), "SELECT * FROM table;");
	}

	@Test
	public void testSelectWithFields() {
		final Select select = new Select("table");
		select.addField("field1");
		select.addField("field2");
		final WhereConditionSet conditionset = new WhereConditionSet();
		conditionset.addCondition(new SimpleWhereCondition("field1", "value1", SqlOperator.EQUALS));
		conditionset.addCondition(SqlOperator.AND, new SimpleWhereCondition("field2", 10, SqlOperator.LESSTHAN));
		conditionset.addCondition(SqlOperator.OR, new SimpleWhereCondition("field2", 1, SqlOperator.BIGGERTHAN));
		select.addCondition(conditionset);
		
		assertEquals(select.build(), "SELECT field1, field2 FROM table WHERE field1 = \"value1\" AND field2 < 10 OR field2 > 1;");
	}
	
	@Test
	public void testInsert() throws ParseException {
		final DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		final Date date = format.parse("2016-01-01 01:00:00");
		
		final Insert sql = new Insert("table");
		sql.addValue(new KeyValuePair("string", "stringvalue"));
		sql.addValue(new KeyValuePair("int", 0));
		sql.addValue(new KeyValuePair("date", date));
		
		assertEquals(sql.build(), "INSERT INTO table (string, int, date) VALUES (\"stringvalue\", 0, \"2016-01-01 01:00:00\");");
	}
	
	@Test
	public void updateTest() {
		final Update update = new Update("table");
		final WhereConditionSet wcs = new WhereConditionSet();
		wcs.addCondition(new SimpleWhereCondition("id", 0, SqlOperator.EQUALS));
		update.addConditions(wcs);
		update.addValue(new KeyValuePair("field", "value"));
		
		assertEquals(update.build(), "UPDATE table SET field=\"value\" WHERE id = 0;");
	}
}
