package com.ivankarez.dokit;

import java.util.HashMap;
import java.util.Map.Entry;

import com.ivankarez.dokit.models.User;
import com.ivankarez.dokit.utils.Utils;

public class SessionManager {
	
	private HashMap<String, User> sessionMap;
	
	public SessionManager() {
		sessionMap = new HashMap<>();
	}
	
	public String addUser(User user) {
		final String sessionId = getRandomSessionId();
		removeUser(user);
		sessionMap.put(sessionId, user);
		return sessionId;
	}

	public void removeUser(User user) {
		if (sessionMap.containsValue(user)) {
			for (Entry<String, User> entry : sessionMap.entrySet()) {
				if (user.equals(entry.getValue())) {
					sessionMap.remove(entry.getKey());
					break;
				}
			}
		}
	}
	
	//TODO: There is chance for id collisions
	private String getRandomSessionId() {
		return Utils.getRandomAlfanumericalString(32);
	}
	
	public User getUserBySessionId(String sessionId) {
		if(sessionMap.containsKey(sessionId)) {
			return sessionMap.get(sessionId);
		}
		return null;
	}
}
