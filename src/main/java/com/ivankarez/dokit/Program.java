package com.ivankarez.dokit;

import static spark.Spark.*;

import java.io.IOException;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ivankarez.dokit.db.DatabaseBuilder;
import com.ivankarez.dokit.db.DatabaseProvider;
import com.ivankarez.dokit.utils.InstanceManager;

import ch.qos.logback.classic.Level;

public class Program {

	static final Logger logger = LoggerFactory.getLogger(Program.class);
	
	public static void main(String[] args) {		
		port(8080);
		initializeLogger();
		try {
			logger.info("=== CREATE INSTANCES ===");
			initializeInstanceManager();
		} catch (Exception e) {
			logger.error("Cannot initialize instances cause: ",e);
		}
		logger.info("=== BUILD DATABASE ===");
		buildDatabase();
		logger.info("=== LOAD SERVICES ===");
		loadServices();
		logger.info("=== DOKIT SERVER STARTING ===");
	}

	private static void initializeLogger() {
		ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger)LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
		root.setLevel(Level.INFO);
	}
	
	private static void initializeInstanceManager() throws IOException, JSONException {
		final Configuration config = Configuration.fromFile("local-config.json");
		
		InstanceManager.putInstance(config);
		InstanceManager.putInstance(new DatabaseProvider(config.dbUsername, config.dbPassword, config.dbAddress, config.dbSchema, config.dbPort));
		InstanceManager.putInstance(new SessionManager());	
	}
	
	private static void buildDatabase() {
		final DatabaseBuilder dbbuilder = new DatabaseBuilder(InstanceManager.getInstance(DatabaseProvider.class));
		dbbuilder.BuildDatabase();
	}
	
	private static void loadServices() {
		Service[] services = ServiceListProvider.getServices();
		for (Service service : services) {
			try {
				get(service.getBindingPath(), (req, res) -> service.fireGet(req, res));
				post(service.getBindingPath(), (req, res) -> service.firePost(req, res));
				logger.info("Service "+service.getClass().getSimpleName() + " is started successfully!");
			} catch (Exception e) {
				logger.error("Service creation failed", e);
			}
		}
	}
}
