package com.ivankarez.dokit.services;

import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ivankarez.dokit.Service;
import com.ivankarez.dokit.db.ProjectDatabaseManager;
import com.ivankarez.dokit.models.Project;
import com.ivankarez.dokit.models.User;
import com.ivankarez.dokit.rendering.DefaultHtmlLayoutBuilder;
import com.ivankarez.dokit.rendering.HtmlAttribute;
import com.ivankarez.dokit.rendering.HtmlElement;
import com.ivankarez.dokit.rendering.HtmlLayoutBuilder;
import com.ivankarez.dokit.rendering.NavbarBuilder;
import spark.Request;
import spark.Response;

public class ProjectService extends Service {

	final static Logger logger = LoggerFactory.getLogger(ProjectService.class);
	final ProjectDatabaseManager manager;
	
	public ProjectService(String bindingPath) {
		super(bindingPath);
		manager = new ProjectDatabaseManager();
	}
	
	@Override
	public String get(Request req, Response res) {
		final User user = getUser();
		if (user == null) return redirect("/");
		
		final HtmlLayoutBuilder builder = new DefaultHtmlLayoutBuilder();
		builder.setTitle("Dokit - Project");
		builder.addScript("js/project.js");
		final NavbarBuilder navbar = new NavbarBuilder();
		navbar.putUser(user);
		builder.addBodyContent(navbar.toString());
		builder.addBodyContent(buildBody());
		
		return builder.toString();
	}
	
	private String buildBody() {
		final HtmlElement root = new HtmlElement("div", "container", null);	
		final HtmlElement row = new HtmlElement("div", "row", root);
		final HtmlElement col = new HtmlElement("div", "col-md-12", row);
		final HtmlElement panel = new HtmlElement("div", "panel panel-default", col);
		final HtmlElement panelHeader = new HtmlElement("div", "panel-heading", panel);
		panelHeader.setText("Project details");
		final HtmlElement panelBody = new HtmlElement("div", "panel-body", panel);
		final HtmlElement bodyRow = new HtmlElement("div", "row", panelBody);
		final HtmlElement bodyCol = new HtmlElement("div", "col-lg-12", bodyRow);
		
		final HtmlElement form = new HtmlElement("form", null, bodyCol);
		form.addAttribute(new HtmlAttribute("id", "project-form"));
		form.addAttribute(new HtmlAttribute("role", "form"));
		form.addAttribute(new HtmlAttribute("style", "display: block;"));
		
		final HtmlElement nameFormGroup = new HtmlElement("div", "form-group", form);
		final HtmlElement nameLabel = new HtmlElement("label", null, nameFormGroup);
		nameLabel.setText("Project name:");
		final HtmlElement nameInput = new HtmlElement("input", "form-control", nameFormGroup);
		nameInput.addAttribute(new HtmlAttribute("type", "text"));
		nameInput.addAttribute(new HtmlAttribute("name", "projectname"));
		nameInput.addAttribute(new HtmlAttribute("id", "projectname"));
		nameInput.addAttribute(new HtmlAttribute("tabindex", "1"));
		nameInput.addAttribute(new HtmlAttribute("placeholder", "Name of your project..."));
		nameInput.addAttribute(new HtmlAttribute("value", ""));
		
		final HtmlElement descriptionFormGroup = new HtmlElement("div", "form-group", form);
		final HtmlElement descriptionLabel = new HtmlElement("label", null, descriptionFormGroup);
		descriptionLabel.setText("Description:");
		final HtmlElement descrInput = new HtmlElement("textarea", "form-control", descriptionFormGroup);
		descrInput.addAttribute(new HtmlAttribute("row", "4"));
		descrInput.addAttribute(new HtmlAttribute("name", "description"));
		descrInput.addAttribute(new HtmlAttribute("id", "description"));
		descrInput.addAttribute(new HtmlAttribute("tabindex", "2"));
		descrInput.addAttribute(new HtmlAttribute("style", "resize:none;"));
		
		final HtmlElement submitFormGroup = new HtmlElement("div", "form-group", form);
		final HtmlElement submitRow = new HtmlElement("div", "row", submitFormGroup);
		final HtmlElement submitColumn = new HtmlElement("div", "col-sm-3 col-sm-offset-9", submitRow);
		final HtmlElement submitInput = new HtmlElement("input", "form-control btn btn-primary", submitColumn);
		submitInput.addAttribute(new HtmlAttribute("type", "submit"));
		submitInput.addAttribute(new HtmlAttribute("name", "submit"));
		submitInput.addAttribute(new HtmlAttribute("id", "submit"));
		submitInput.addAttribute(new HtmlAttribute("tabindex", "4"));
		submitInput.addAttribute(new HtmlAttribute("value", "save"));
		
		return root.toString();
	}

	@Override
	public String post(Request req, Response res) {
		final User user = getUser();
		if (user == null)
			return this.buildSimpleResult(1, "No user logged in!");
		final Project project = parseFromRequest(req);
		if(project == null) {
			return buildSimpleResult(1, "Wrong data!");
		}
		if (project.getId() == -1) {
			final Project projectToSave = new Project(-1, project.getName(), project.getDescription(), new Date(),
					user.getId());
			final boolean success = manager.addProject(projectToSave);
			if (success) {
				return buildSimpleResult(0, "OK");
			} else {
				return buildSimpleResult(1, "Cannot save project at the moment!");
			}
		} else {
			return buildSimpleResult(1, "Method not implemented yet!");
		}
	}
	
	private Project parseFromRequest(Request req) {
		try {
			JSONObject json = new JSONObject(req.body());
			
			int id = -1;
			String name = "";
			String description = "";
			Date createdAt = new Date();
			int creatorId = -1;
			
			if(json.has("id")) {
				id = json.getInt("id");
			}
			if(json.has("name")) {
				name = json.getString("name");
			}
			if(json.has("description")) {
				description = json.getString("description");
			}
			
			return new Project(id, name, description, createdAt, creatorId);
		} catch (JSONException e) {
			logger.error("Cannot parse json: ", e);
			return null;
		}
	}
}
