package com.ivankarez.dokit.services;

import org.json.JSONObject;

import com.ivankarez.dokit.Service;
import com.ivankarez.dokit.db.UserDataManager;
import com.ivankarez.dokit.rendering.DefaultHtmlLayoutBuilder;
import com.ivankarez.dokit.rendering.HtmlLayoutBuilder;
import com.ivankarez.dokit.rendering.NavbarBuilder;
import com.ivankarez.dokit.rendering.TemplateReader;
import com.ivankarez.dokit.utils.MethodResult;

import spark.Request;
import spark.Response;

public class ActivateService extends Service {

	private final String activateForm;
	private UserDataManager dataManager;
	
	public ActivateService(String bindingPath) {
		super(bindingPath);
		activateForm = new TemplateReader("templates/activate.html").read();
		dataManager = new UserDataManager();
	}

	@Override
	public String post(Request req, Response res) {
		try{
			final JSONObject json = new JSONObject(req.body());
			final String username = json.getString("username");
			final String activationCode = json.getString("activationcode");
			final MethodResult result = dataManager.activateUser(username, activationCode);
			if(result.wasError()) {
				return buildSimpleResult(1, result.getErrorMessage());
			}
			return buildSimpleResult(0, "OK");
		} catch (Exception e) {
			return buildSimpleResult(2, "Unkown error.");
		}
	}

	@Override
	public String get(Request req, Response res) {
		if(getUser() != null) {
			redirect("/");
		}
		final HtmlLayoutBuilder builder = new DefaultHtmlLayoutBuilder();
		builder.setTitle("Dokit - Login");
		builder.addScript("js/activate.js");
		builder.addStyle("style/login.css");
		final NavbarBuilder navbar = new NavbarBuilder();
		navbar.putUser(null);
		builder.addBodyContent(navbar.toString());
		builder.addBodyContent(activateForm);
		return builder.toString();
	}

}
