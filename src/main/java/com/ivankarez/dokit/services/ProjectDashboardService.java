package com.ivankarez.dokit.services;

import com.ivankarez.dokit.Service;
import com.ivankarez.dokit.db.ProjectDatabaseManager;
import com.ivankarez.dokit.models.Project;
import com.ivankarez.dokit.rendering.DefaultHtmlLayoutBuilder;
import com.ivankarez.dokit.rendering.HtmlAttribute;
import com.ivankarez.dokit.rendering.HtmlElement;
import com.ivankarez.dokit.rendering.HtmlLayoutBuilder;
import com.ivankarez.dokit.rendering.NavbarBuilder;
import com.ivankarez.dokit.utils.Utils;

import spark.Request;
import spark.Response;

public class ProjectDashboardService extends Service {

	final ProjectDatabaseManager db;
	
	public ProjectDashboardService(String bindingPath) {
		super(bindingPath);
		db = new ProjectDatabaseManager();
	}

	@Override
	public String post(Request req, Response res) {
		return "";
	}

	@Override
	public String get(Request req, Response res) {
		if(getUser() == null) return redirect("/login");
		try {
			final int projectId = Integer.parseInt(req.params(":id"));
			final Project current = db.getProjectById(projectId);					
			final HtmlLayoutBuilder builder = new DefaultHtmlLayoutBuilder();
			final NavbarBuilder navbarBuilder = new NavbarBuilder("/");
			//builder.addScript("js/project.js");
			navbarBuilder.putUser(getUser());
			builder.setTitle("Dokit - " + current.getName());
			builder.addBodyContent(navbarBuilder.toString());
			builder.addBodyContent(buildPage(current));
			return builder.toString();
		} catch(Exception e) {
			return redirect("/");
		}		
	}
	
	private String buildPage(Project model) {
		final HtmlElement root = new HtmlElement("div", "container", null);
		final HtmlElement row = new HtmlElement("div","row", root);
		
		final HtmlElement column2 = new HtmlElement("div", "col-md-2", row);
		final HtmlElement panelArticles = new HtmlElement("div", "panel panel-default", column2);
		final HtmlElement panelArticlesHeader = new HtmlElement("div", "panel-heading", panelArticles);
		panelArticlesHeader.setText("Articles");
		final HtmlElement panelArticlesBody = new HtmlElement("div", "panel-body", panelArticles);
		panelArticlesBody.setText("ASdfsfds\nsefiosjfies\nfioesjgfiopesj\nfioesjfioesjfios\niofesjfioesjfios\nfioesjfioesjfeois\nifoesjfeiosjfeois\njfeiosjfioesjfioes\nfeiosjgiorispofjesop");
		
		final HtmlElement column = new HtmlElement("div", "col-md-10", row);
		final HtmlElement panelRoot = new HtmlElement("div", "panel panel-default", column);
		final HtmlElement panelHeader = new HtmlElement("div", "panel-heading", panelRoot);
		panelHeader.setText("Project details");
		final HtmlElement panelBody = new HtmlElement("div", "panel-body", panelRoot);
		
		final HtmlElement projectName = new HtmlElement("h3", null, panelBody);
		projectName.setText(model.getName());
		attachParameter(panelBody, "Created at:", Utils.dateToString(model.getCreatedAt()));
		attachParameter(panelBody, "Description:", model.getDescription());

		return root.toString();
	}
	
	private void attachParameter(HtmlElement parent, String name, String value) {
		final HtmlElement parameter = new HtmlElement("div", "row", parent);
		parameter.addAttribute(new HtmlAttribute("style", "margin-top:25px;"));
		final HtmlElement leftPanel = new HtmlElement("div", "col-md-4", parameter);
		final HtmlElement strong = new HtmlElement("b", null, leftPanel);
		strong.setText(name);
		final HtmlElement rightPanel = new HtmlElement("div", "col-md-4", parameter);
		rightPanel.setText(value);
	}

}
