package com.ivankarez.dokit.services;

import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ivankarez.dokit.Service;
import com.ivankarez.dokit.SessionManager;
import com.ivankarez.dokit.db.LoginOutcome;
import com.ivankarez.dokit.db.RegistrationOutcome;
import com.ivankarez.dokit.db.UserDataManager;
import com.ivankarez.dokit.models.User;
import com.ivankarez.dokit.rendering.DefaultHtmlLayoutBuilder;
import com.ivankarez.dokit.rendering.HtmlLayoutBuilder;
import com.ivankarez.dokit.rendering.NavbarBuilder;
import com.ivankarez.dokit.rendering.TemplateReader;
import com.ivankarez.dokit.utils.InstanceManager;
import com.ivankarez.dokit.utils.Utils;

import spark.Request;
import spark.Response;

public class LoginService extends Service {

	final static Logger logger = LoggerFactory.getLogger(LoginService.class);
	private final String loginForm;
	private final UserDataManager dataManager;

	public LoginService(String bindingPath) {
		super(bindingPath);
		loginForm = new TemplateReader("templates/login.html").read();
		dataManager = new UserDataManager();
	}

	@Override
	public String post(Request req, Response res) {
		final String method = req.queryParams("method");
		if (method.equals("registration")) {
			return registrateUser(req);
		} else if (method.equals("login")) {
			return loginUser(req, res);
		}
		logger.error("Illegal state. Login method name not registration or login.");
		return buildInternalErrorResponse();
	}

	public String registrateUser(Request req) {
		try {
			final User user = fetchUserData(req);
			final RegistrationOutcome result = dataManager.insertUser(user);
			switch (result) {
			case EmailIsTaken:
				return buildSimpleResult(1, "Email address is taken!");
			case UsernameIsTaken:
				return buildSimpleResult(1, "Username is taken!");
			default:
				return buildSimpleResult(0, "OK");
			}
		} catch (Exception e) {
			logger.error("Exception while registrate user: ", e);
			return buildInternalErrorResponse();
		}
	}

	@SuppressWarnings("incomplete-switch")
	public String loginUser(Request req, Response res) {
		try {
			final JSONObject json = new JSONObject(req.body());
			final String username = json.getString("username");
			final String password = json.getString("password");
			final boolean remember = json.getString("remember").equals("on");
			final LoginOutcome result = dataManager.login(username, password);
			switch (result) {
			case UserNotActivated:
				return buildSimpleResult(1, "User not activated.");
			case WrongPassword:
				return buildSimpleResult(1, "Wrong password!");
			case UserNotExist:
				return buildSimpleResult(1, "User does not exist!");
			}
			final User loggedInUser = dataManager.getUserByUsername(username);
			final SessionManager manager = InstanceManager.getInstance(SessionManager.class);
			final String sessionId = manager.addUser(loggedInUser);
			if (remember) {
				final String hash = Utils.SHA256(loggedInUser.getPasswordhash() + loggedInUser.getEmailAddress());
				final String token = loggedInUser.getUsername() + ":" + hash;
				res.cookie("token", token, 2678400);
			}
			JSONObject returnJson = new JSONObject();
			returnJson.append("result", 0);
			returnJson.append("sessionid", sessionId);
			return returnJson.toString();
		} catch (Exception e) {
			logger.error("Can't parse user from json: ", e);
			return buildInternalErrorResponse();
		}
	}

	protected User fetchUserData(Request req) throws JSONException {
		JSONObject json = new JSONObject(req.body());

		final String username = json.getString("username");
		final String email = json.getString("email");
		final String fullname = json.getString("name");
		final String password = json.getString("password");
		final String passwordHash = Utils.SHA256(password);
		final String activationCode = Utils.getRandomAlfanumericalString(16);
		final User user = new User(-1, email, username, fullname, passwordHash, activationCode, false, new Date());
		return user;
	}

	@Override
	public String get(Request req, Response res) {
		if (getUser() != null) {
			return redirect("/");
		}
		final String token = req.cookie("token");
		if (token != null) {
			if (tryToLoginWithToken(token, res)) {
				return redirect("/");
			}
		}
		final HtmlLayoutBuilder builder = new DefaultHtmlLayoutBuilder();
		builder.setTitle("Dokit - Login");
		builder.addScript("js/login.js");
		builder.addStyle("style/login.css");
		final NavbarBuilder navbar = new NavbarBuilder();
		navbar.putUser(null);
		builder.addBodyContent(navbar.toString());
		builder.addBodyContent(loginForm);
		return builder.toString();
	}

	protected boolean tryToLoginWithToken(String token, Response res) {
		try {
			final String username = getUsernameFromToken(token);
			final User user = dataManager.getUserByUsername(username);
			if (user == null || !user.isActive()) {
				return false;
			}
			final String hash = token.substring(username.length() + 1);
			final String localHash = Utils.SHA256(user.getPasswordhash() + user.getEmailAddress());
			if (!hash.equals(localHash)) {
				return false;
			}
			final SessionManager manager = InstanceManager.getInstance(SessionManager.class);
			final String sessionId = manager.addUser(user);
			res.cookie("session", sessionId);
			res.cookie("token", token, 2678400);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	protected String getUsernameFromToken(String token) {
		final StringBuilder sb = new StringBuilder();
		for (int i = 0; i < token.length(); i++) {
			if (token.charAt(i) == ':') {
				break;
			}
			sb.append(token.charAt(i));
		}
		return sb.toString();
	}
}
