package com.ivankarez.dokit;

import com.ivankarez.dokit.services.*;

public class ServiceListProvider {
	
	public static Service[] getServices() {
		return new Service[] {
			new SimpleFileProviderService("style/*"),
			new SimpleFileProviderService("js/*"),
			new SimpleFileProviderService("fonts/*"),
			new IndexService("/"),
			new LoginService("/login"),
			new ActivateService("/activate"),
			new ProjectService("/project"),
			new ProjectDashboardService("/project/:id")
		};		
	}
	
}
