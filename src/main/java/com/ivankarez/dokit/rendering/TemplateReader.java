package com.ivankarez.dokit.rendering;

import java.io.IOException;

import spark.utils.IOUtils;

public class TemplateReader {
	
	final String templateName;
	
	public TemplateReader(String templateName) {
		this.templateName = templateName;
	}
	
	public String read() {
		final ClassLoader classLoader = NavbarBuilder.class.getClassLoader();
		String result = "";
		try {
			result = IOUtils.toString(classLoader.getResourceAsStream(templateName));
		} catch (IOException e) {
			
		}
		return result;
	}
	
}
