package com.ivankarez.dokit.rendering;

public class DefaultHtmlLayoutBuilder extends HtmlLayoutBuilder {

	public DefaultHtmlLayoutBuilder(){
		super();
		addStyle("/style/bootstrap.min.css");
		addScript("/js/jquery-3.1.1.min.js");
		addScript("/js/bootstrap.min.js");
	}
}
