package com.ivankarez.dokit.rendering;

import java.util.ArrayList;

public class HtmlElement {
	
	private final ArrayList<HtmlElement> childElements;
	private final ArrayList<HtmlAttribute> attributes;
	private String name;
	private String text;
	
	public HtmlElement(String name, String clazz, HtmlElement parent) {
		this(name);
		if (clazz != null)
			addAttribute(new HtmlAttribute("class", clazz));
		if (parent != null)
			parent.addChildren(this);
	}
	
	public HtmlElement(String name) {
		childElements = new ArrayList<HtmlElement>();
		attributes = new ArrayList<HtmlAttribute>();
		this.name = name;
		this.text = "";
	}
	
	public HtmlElement addChildren(HtmlElement children) {
		childElements.add(children);
		return this;
	}
	
	public HtmlElement addAttribute(HtmlAttribute attribute) {
		attributes.add(attribute);
		return this;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		
		sb.append("<"+name);
		for (HtmlAttribute htmlAttribute : attributes) {
			sb.append(" ");
			sb.append(htmlAttribute.toString());
		}
		sb.append(">");
		if(text != "") {
			sb.append(text);
		}
		for (HtmlElement children : childElements) {
			sb.append(children.toString());
		}
		sb.append("</"+name+">");
		return sb.toString();
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
