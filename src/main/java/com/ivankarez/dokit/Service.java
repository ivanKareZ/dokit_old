package com.ivankarez.dokit;

import spark.Request;
import spark.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ivankarez.dokit.models.User;
import com.ivankarez.dokit.utils.InstanceManager;

public abstract class Service {

	final static String SESSION_COOKIE = "session";
	final static Logger logger = LoggerFactory.getLogger(Service.class);
	private User user;
	
	protected final String bindingPath;

	public Service(String bindingPath) {
		this.bindingPath = bindingPath;
	}

	public String getBindingPath() {
		return bindingPath;
	}
	
	public String firePost(Request req, Response res) {
		user = getLoggedInUser(req);
		logger.info(String.format("POST: " + req.url()));
		return this.post(req, res);
	}
	
	public String fireGet(Request req, Response res) {
		user = getLoggedInUser(req);
		logger.info(String.format("GET: " + req.url()));
		return this.get(req, res);
	}

	public abstract String post(Request req, Response res);

	public abstract String get(Request req, Response res);
	
	protected User getLoggedInUser(Request req) {
		final String session = req.cookie(SESSION_COOKIE);
		return InstanceManager.getInstance(SessionManager.class).getUserBySessionId(session);
	}

	protected String redirect(String path) {
		return "<script type='text/javascript'>window.location = '"+path+"';</script>";
	}
	
	protected String buildSimpleResult(int errorCode, String message) {
		return "{ \"result\":\""+errorCode+"\", \"message\":\""+message+"\"}";
	}
	
	protected String buildInternalErrorResponse() {
		return buildSimpleResult(1, "Internal server error, please try again later!");
	}
	
	public User getUser() {
		return user;
	}
}
