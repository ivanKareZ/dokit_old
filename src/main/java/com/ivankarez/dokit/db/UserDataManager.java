package com.ivankarez.dokit.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ivankarez.dokit.db.sql.Insert;
import com.ivankarez.dokit.db.sql.KeyValuePair;
import com.ivankarez.dokit.db.sql.Select;
import com.ivankarez.dokit.db.sql.SimpleWhereCondition;
import com.ivankarez.dokit.db.sql.SqlOperator;
import com.ivankarez.dokit.db.sql.WhereConditionSet;
import com.ivankarez.dokit.models.User;
import com.ivankarez.dokit.utils.MethodResult;
import com.ivankarez.dokit.utils.Utils;

public class UserDataManager extends DataManager {
	
	final static Logger logger = LoggerFactory.getLogger(UserDataManager.class);
	
	public User getUserById(int id) {
		return null;
	}
	
	public RegistrationOutcome insertUser(User user) throws SQLException {
		if (isFieldTaken("username", user.getUsername())) {
			return RegistrationOutcome.UsernameIsTaken;
		}
		if (isFieldTaken("email", user.getEmailAddress())) {
			return RegistrationOutcome.EmailIsTaken;
		}
		
		final Insert insert = new Insert("user");
		insert.addValue(new KeyValuePair("username", user.getUsername()));
		insert.addValue(new KeyValuePair("email", user.getEmailAddress()));
		insert.addValue(new KeyValuePair("name", user.getFullname()));
		insert.addValue(new KeyValuePair("password", user.getPasswordhash()));
		insert.addValue(new KeyValuePair("activationcode", user.getActivationCode()));
		insert.addValue(new KeyValuePair("registrationdate", user.getRegistrationDate()));		
		executeNonQuery(insert.build());
		return RegistrationOutcome.OK;
	}
	
	public boolean isFieldTaken(String field ,String value) throws SQLException {
		final Select select = new Select("user");
		select.addField("count(*)");
		final WhereConditionSet conditionset = new WhereConditionSet();
		conditionset.addCondition(new SimpleWhereCondition(field, value, SqlOperator.EQUALS));
		select.addCondition(conditionset);
		final String sql = select.build();
		final ResultSet result = executeQuery(sql);
		result.next();
		final int count = result.getInt(1);
		result.close();
		return count != 0;
	}
	
	public MethodResult activateUser(String username, String activationCode) {
		final Select select = new Select("user");
		select.addField("activationcode");
		final WhereConditionSet conditionset = new WhereConditionSet();
		conditionset.addCondition(new SimpleWhereCondition("username", username, SqlOperator.EQUALS));
		select.addCondition(conditionset);
		final String sql = select.build();
		try(final ResultSet result = executeQuery(sql)) {		
			if(result.next()){
				final String dbActivationCode = result.getString(1);
				if(dbActivationCode.equals(activationCode)) {					
					final String updateSql = "UPDATE user SET active='1' WHERE username='"+username+"';";
					executeNonQuery(updateSql);
					return new MethodResult(false, "OK");
				} else {
					return new MethodResult(true, "Wrong activation code");
				}
			} else {
				return new MethodResult(true, "Username does not exist!");
			}
		} catch (Exception e) {
			logger.error("Cannot insert user to the database, cause: ", e);
			return new MethodResult(true, "Unkown database error!");
		}
	}

	public LoginOutcome login(String username, String password) throws SQLException {
		final Select select = new Select("user");
		final WhereConditionSet set = new WhereConditionSet();
		set.addCondition(new SimpleWhereCondition("username", username, SqlOperator.EQUALS));
		select.addCondition(set);
		select.addField("password");
		select.addField("active");

		final String passwordHash = Utils.SHA256(password);
		final String sql = select.build();
		final ResultSet result = executeQuery(sql);
		if (result.next()) {
			if(result.getInt("active") != 1){
				return LoginOutcome.UserNotActivated;
			}
			if (!result.getString("password").equals(passwordHash)) {
				return LoginOutcome.WrongPassword;
			}
		} else {
			return LoginOutcome.UserNotExist;
		}
		return LoginOutcome.OK;
	}

	public User getUserByUsername(String pusername) {
		final Select select = new Select("user");
		final WhereConditionSet set = new WhereConditionSet();
		set.addCondition(new SimpleWhereCondition("username", pusername, SqlOperator.EQUALS));
		select.addCondition(set);
		
		final String sql = select.build();
		try(final ResultSet result = executeQuery(sql)) {		
			if(result.next()){
				final int id = result.getInt("id");
				final String username = result.getString("username");
				final String email = result.getString("email");
				final String name = result.getString("name");
				final String password = result.getString("password");
				final String activationcode = result.getString("activationcode");
				final boolean active = result.getInt("active") == 1;
				final Date regDate = result.getDate("registrationdate");
				return new User(id, email, username, name, password, activationcode, active, regDate);
			}
		} catch (Exception e) {
			logger.error("Cannot insert user to the database, cause: ", e);
		}
		return null;
	}
}
