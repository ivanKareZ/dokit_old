package com.ivankarez.dokit.db;

public enum RegistrationOutcome {
	OK, UsernameIsTaken, EmailIsTaken
}
