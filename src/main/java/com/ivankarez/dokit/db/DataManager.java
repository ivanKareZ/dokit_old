package com.ivankarez.dokit.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.ivankarez.dokit.utils.InstanceManager;

public abstract class DataManager {

	final private Connection conn;

	public DataManager() {
		conn = InstanceManager.getInstance(DatabaseProvider.class).getConnection();
	}

	protected ResultSet executeQuery(String query) throws SQLException {
		final Statement statement = conn.createStatement();
		final ResultSet resultSet = statement.executeQuery(query);
		return resultSet;
	}

	protected boolean executeNonQuery(String query) throws SQLException {
		try(final Statement statement = conn.createStatement()){
			final boolean result = statement.execute(query);
			return result;
		}
	}
	
	protected int executeAndGetID(String query) {
		try (final PreparedStatement statement = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
			final int affectedRows = statement.executeUpdate();
			if (affectedRows == 0) {
				return -1;
			}
			try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					return generatedKeys.getInt(1);
				} else {
					throw new SQLException("Creating user failed, no ID obtained.");
				}
			}
		} catch (SQLException e) {
			return -1;
		}
	}

}
