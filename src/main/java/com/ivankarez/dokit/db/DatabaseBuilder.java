package com.ivankarez.dokit.db;

import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DatabaseBuilder {

	private static final Logger logger = LoggerFactory.getLogger(DatabaseBuilder.class);
	private final DatabaseProvider db;
	
	public DatabaseBuilder(DatabaseProvider db) {
		this.db = db;
	}
	
	protected boolean executeNonQuery(String query) throws SQLException {
		try(final Statement statement = db.getConnection().createStatement()){
			final boolean result = statement.execute(query);
			return result;
		}
	}
	
	public void BuildDatabase() {
		user();
		project();
		projectAccess();
	}
	
	private void createTable(String sql, String name) {
		try {
			executeNonQuery(sql);
			logger.info("Create table "+name+"!");
		} catch (SQLException e) {
			if (!e.getMessage().contains("already exists")) {
				logger.error("Cannot create table "+name+" cause: ", e);
			} else {
				logger.info("Table "+name+" is already exist. Skipping creation...");
			}
		}
	}
	
	private void user() {
		String sql = "CREATE TABLE `user` (  `id` int(11) NOT NULL AUTO_INCREMENT,  `username` varchar(100) NOT NULL,  `email` varchar(100) NOT NULL,  `name` varchar(150) NOT NULL,  `password` varchar(128) NOT NULL,  `activationcode` varchar(16) NOT NULL,  `active` tinyint(4) NOT NULL DEFAULT '0',  `registrationdate` datetime NOT NULL,  PRIMARY KEY (`id`),  UNIQUE KEY `username_UNIQUE` (`username`),  UNIQUE KEY `email_UNIQUE` (`email`)) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;";
		createTable(sql, "user");
	}
	
	private void project() {
		String sql = "CREATE TABLE `project` ( `id` INT NOT NULL AUTO_INCREMENT, `name` VARCHAR(100) NOT NULL, `description` VARCHAR(1000) NULL, `created` DATETIME NOT NULL, `creatorid` INT NOT NULL, PRIMARY KEY (`id`));";
		createTable(sql, "project");
	}

	private void projectAccess() {
		String sql = "CREATE TABLE `project_access` ( `id` int(11) NOT NULL AUTO_INCREMENT, `userid` int(11) NOT NULL, `projectid` int(11) NOT NULL, `level` int(11) NOT NULL DEFAULT '0', PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
		createTable(sql, "project_access");
	}
	
}


