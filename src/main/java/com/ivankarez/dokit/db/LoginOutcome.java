package com.ivankarez.dokit.db;

public enum LoginOutcome {
	OK, UserNotExist, WrongPassword, UserNotActivated
}
