package com.ivankarez.dokit.db.sql;

import java.util.ArrayList;

public class Insert extends SqlExpression {

	final private ArrayList<KeyValuePair> kvs;
	final private String tableName;
	
	public Insert(String tableName) {
		this.tableName = tableName;
		kvs = new ArrayList<>();
	}
	
	public void addValue(KeyValuePair kvp) {
		kvs.add(kvp);
	}
	
	@Override
	public String build() {
		final StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO ");
		sb.append(tableName);
		sb.append(" (");
		for (int i = 0; i < kvs.size(); i++) {
			sb.append(kvs.get(i).getKey());
			if(i != kvs.size() - 1) {
				sb.append(", ");
			}
		}
		sb.append(") VALUES (");
		for (int i = 0; i < kvs.size(); i++) {
			sb.append(kvs.get(i).getValue());
			if(i != kvs.size() - 1) {
				sb.append(", ");
			}
		}
		sb.append(");");
		return sb.toString();
	}

}
