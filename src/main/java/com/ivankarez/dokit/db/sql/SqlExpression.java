package com.ivankarez.dokit.db.sql;

public abstract class SqlExpression {

	public abstract String build();
	
}
