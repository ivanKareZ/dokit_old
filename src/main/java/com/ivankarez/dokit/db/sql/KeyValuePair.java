package com.ivankarez.dokit.db.sql;

import java.util.Date;

import com.ivankarez.dokit.utils.Utils;

public class KeyValuePair {

	final private String key;
	final private String value;
	
	public String getKey() {
		return key;
	}

	public String getValue() {
		return value;
	}

	public KeyValuePair(String key, String value) {
		this.key = key;
		this.value = "\""+value+"\"";
	}
	
	public KeyValuePair(String key, int value) {
		this.key = key;
		this.value = Integer.toString(value);
	}
	
	public KeyValuePair(String key, Date value) {
		this.key = key;
		this.value = "\""+Utils.dateToString(value)+"\"";
	}
	
}
