package com.ivankarez.dokit.db.sql;

public class WhereConditionSet {
	
	private final StringBuilder builder;
	
	public WhereConditionSet() {
		builder = new StringBuilder();
	}
	
	public void addCondition(SqlOperator appender, SimpleWhereCondition condition) {
		if(appender != null) {
			builder.append(" ");
			builder.append(appender.getRepresentation());
			builder.append(" ");
		}
		builder.append(condition);
	}
	
	public void addCondition(SimpleWhereCondition condition) {
		addCondition(null ,condition);
	}
	
	@Override
	public String toString() {
		return builder.toString();
	}
	
}
