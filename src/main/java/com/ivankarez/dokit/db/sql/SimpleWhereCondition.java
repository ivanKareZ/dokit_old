package com.ivankarez.dokit.db.sql;

import java.util.Date;

import com.ivankarez.dokit.utils.Utils;

public class SimpleWhereCondition {

	private final String condition;
	
	public SimpleWhereCondition(String fieldName, String value, SqlOperator operator) {
		final StringBuilder sb = new StringBuilder();
		sb.append(fieldName);
		sb.append(" ");
		sb.append(operator.getRepresentation());
		sb.append(" ");
		sb.append("\"");
		sb.append(value);
		sb.append("\"");
		condition = sb.toString();
	}
	
	public SimpleWhereCondition(String fieldName, int value, SqlOperator operator) {
		final StringBuilder sb = new StringBuilder();
		sb.append(fieldName);
		sb.append(" ");
		sb.append(operator.getRepresentation());
		sb.append(" ");
		sb.append(value);
		condition = sb.toString();
	}
	
	public SimpleWhereCondition(String fieldName, Date value, SqlOperator operator) {
		final StringBuilder sb = new StringBuilder();
		sb.append(fieldName);
		sb.append(" ");
		sb.append(operator.getRepresentation());
		sb.append(" ");
		sb.append("\"");
		sb.append(Utils.dateToString(value));
		sb.append("\"");
		condition = sb.toString();
	}
	
	@Override
	public String toString() {
		return this.condition;
	}
}
