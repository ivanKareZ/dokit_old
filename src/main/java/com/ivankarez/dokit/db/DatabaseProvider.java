package com.ivankarez.dokit.db;

import java.sql.Connection;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class DatabaseProvider {

	final static Logger logger = LoggerFactory.getLogger(DatabaseProvider.class);
	
	protected final String username;
	protected final String password;
	protected final String serverAddress;	
	protected final String schema;
	protected final int port;
	protected final MysqlDataSource dataSource;
	protected Connection conn;
	
	public DatabaseProvider(String username, String password, String serverAddress, String dbname, int port) {
		this.username = username;
		this.password = password;
		this.serverAddress = serverAddress;
		this.port = port;
		this.schema = dbname;
		this.dataSource = new MysqlDataSource();
		dataSource.setUser(username);
		dataSource.setPassword(password);
		dataSource.setServerName(serverAddress);
		dataSource.setPort(port);
		dataSource.setDatabaseName(dbname);
		try {
			conn = dataSource.getConnection();
		} catch (SQLException e) {
			logger.error("Can't connect to database!", e);
		}
	}

	
	public Connection getConnection() {
		return conn;
	}
}
