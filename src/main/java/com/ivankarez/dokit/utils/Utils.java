package com.ivankarez.dokit.utils;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Random;

public class Utils {
	
	final static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	public static String getRandomAlfanumericalString(int length) {
		final Random r = new Random();
		final String possibleChars = "qwertzuiopasdfghjklyxcvbnm0123456789QWERTZUIOASDFGHJKLYXCVBNM";
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < length; i++) {
			final int randomIndex = r.nextInt(possibleChars.length());
			sb.append(possibleChars.charAt(randomIndex));
		}
		return sb.toString();
	}
	
	public static String SHA256(String input) {
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("SHA-256");
			byte[] hash = digest.digest(input.getBytes(StandardCharsets.UTF_8));
			return Base64.getEncoder().encodeToString(hash);
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("No method named SHA-256");
		}
	}
	
	public static String dateToString(Date date) {
		return sdf.format(date);
	}
	
	public static String trimIfLonger(String s, int max) {
		if(s.length() < max) return s;
		return s.substring(0, max) + "...";
	}
	
}
