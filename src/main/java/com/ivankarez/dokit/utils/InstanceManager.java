package com.ivankarez.dokit.utils;

import java.util.HashMap;

public class InstanceManager {
	
	@SuppressWarnings("rawtypes")
	protected static HashMap<Class, Object> instances;
	
	static{
		instances = new HashMap<>();
	}
	
	public static void putInstance(Object obj) {
		instances.put(obj.getClass(), obj);
	}
	
	@SuppressWarnings({ "unchecked" })
	public static <T> T getInstance(Class<T> clazz) {
		return (T) instances.get(clazz);
	}
	
}