package com.ivankarez.dokit.utils;

public class MethodResult {
	private boolean wasError;
	private String errorMessage;
	
	public MethodResult(boolean wasError, String errorMessage) {
		super();
		this.wasError = wasError;
		this.errorMessage = errorMessage;
	}
	public boolean wasError() {
		return wasError;
	}
	public void setWasError(boolean wasError) {
		this.wasError = wasError;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}
