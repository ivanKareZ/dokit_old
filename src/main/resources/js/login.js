$(function() {

    $('#login-form-link').click(function(e) {
		$("#login-form").delay(100).fadeIn(100);
 		$("#register-form").fadeOut(100);
		$('#register-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});
	$('#register-form-link').click(function(e) {
		$("#register-form").delay(100).fadeIn(100);
 		$("#login-form").fadeOut(100);
		$('#login-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});
	$('#register-form').attr("onSubmit", "return validateReg()");
	$('#login-form').attr("onSubmit", "return validateLogin()");

});

function validateReg() {
	var password = $("#reg-password").val();
	var password2 = $("#confirm-password").val();
	var username = $("#reg-username").val();
	var fullname = $("#fullname").val();
	var email = $("#email").val();
	
	var errors = [];
	
	if(password !== password2) {
		errors.push("The two password must be the same!");
	}
	if(password.length < 6) {
		errors.push("Password must be longer than 6 character!");
	}
	if(username.length < 6) {
		errors.push("Username must be longer than 6 character!");
	}
	if(fullname.length < 1) {
		errors.push("Fullname is required!");
	}
	
	if(errors.length > 0) {
		var errorText = "<ul>";
		for (var i = 0; i < errors.length; i++) {
			errorText+= "<li>"+errors[i]+"</li>";
		}
		errorText += "</ul>";
		showError(errorText);
	} else {
		var data = {};
		data.password = password;
		data.username = username;
		data.name = fullname;
		data.email = email;
		$.ajax({
		    url: '/login?method=registration',
		    type: 'POST',
		    data: JSON.stringify(data),
		    contentType: 'application/json; charset=utf-8',
		    dataType: 'json',
		    async: true,
		    success: function(msg) {
		        if(msg.result != 0) {
		        	showError(msg.message);
		        }
		        else {
		        	window.location.replace("/login");
		        }
		    },
	      	error: function (xhr, ajaxOptions, thrownError) {
	      		showError("Unkown error. ("+xhr.status+")");
	      	}
		});
	}
	return false;
}

function validateLogin() {
	var errors = [];
	
	var password = $("#password").val();
	var username = $("#username").val();
	var remember = $("#remember").val();
	
	if(username == ""){
		errors.push("Username required!");
	}
	if(password == ""){
		errors.push("Password required!");
	}
	if(errors.length > 0) {
		var errorText = "<ul>";
		for (var i = 0; i < errors.length; i++) {
			errorText+= "<li>"+errors[i]+"</li>";
		}
		errorText += "</ul>";
		showError(errorText);
	} else {
		var data = {};
		data.password = password;
		data.username = username;
		data.remember = remember;
		$.ajax({
		    url: '/login?method=login',
		    type: 'POST',
		    data: JSON.stringify(data),
		    contentType: 'application/json; charset=utf-8',
		    dataType: 'json',
		    async: true,
		    success: function(msg) {
		        if(msg.result != 0) {
		        	showError(msg.message);
		        }
		        else {
		        	document.cookie = "session="+msg.sessionid;
		        	window.location.replace("/");
		        }
		    },
	      	error: function (xhr, ajaxOptions, thrownError) {
	      		showError("Unkown error. ("+xhr.status+")");
	      	}
		});
	}
	return false;
}

function hideError() {
	$("#error-text").html("");
	$("#error-panel").hide("fast", function() {
		$("#error-panel").css('visibility', 'hidden');
	});
}

function showError(text)  {
	$("#error-text").html(text);
	$("#error-panel").css('visibility', 'visible');
	$("#error-panel").show("fast", function() {
		
	});
}
