$(function() {
	$('#activation-form').attr("onSubmit", "return validate()");
});

function validate() {
	var activationCode = $("#activation-code").val();
	var username = $("#username").val();
	
	if(username == "") {
		showError("You did not enter your user name.");
		return false;
	}
	if(activationCode == ""){
		showError("You did not enter your activation code.");
		return false;
	}
	
	var data = {};
	data.activationcode = activationCode;
	data.username = username;
	$.ajax({
	    url: '/activate',
	    type: 'POST',
	    data: JSON.stringify(data),
	    contentType: 'application/json; charset=utf-8',
	    dataType: 'json',
	    async: true,
	    success: function(msg) {
	        if(msg.result != 0) {
	        	showError(msg.message);
	        }
	        else {
	        	window.location.replace("/login");
	        }
	    },
      	error: function (xhr, ajaxOptions, thrownError) {
      		showError("Unkown error. ("+xhr.status+")");
      	}
	});
	
	return false;
}

function hideError() {
	$("#error-text").html("");
	$("#error-panel").hide("fast", function() {
		$("#error-panel").css('visibility', 'hidden');
	});
}

function showError(text)  {
	$("#error-text").html(text);
	$("#error-panel").css('visibility', 'visible');
	$("#error-panel").show("fast", function() {
		
	});
}